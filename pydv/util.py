import logging
import numpy as np

from . import const


def get_best_subplot_dimensions(num):
    rows = int(np.floor(np.sqrt(num)))
    cols = int(np.ceil(float(num)/rows))
    return rows, cols


def make_default_logger(name):
    logger = logging.getLogger(name)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler = logging.NullHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)
    return logger


class StateInterval(object):
    def __init__(self, state, start_time, end_time):
        self.state = state
        self.start_time = start_time
        self.end_time = end_time


def _get_next_state_signal(state_signal_data, start_idx):
    i = start_idx
    num_converged_points = 0
    settled_state_signal = None
    while i < len(state_signal_data):
        state_signal = state_signal_data[i]
        rounded_state_signal = round(state_signal)
        # if state_signal is near an integer, check to see if it is near the same
        # integer as it was on the last loop iteration
        if rounded_state_signal - const.WD_STATE_CONVERGING_EPSILON\
                < state_signal\
                < rounded_state_signal + const.WD_STATE_CONVERGING_EPSILON:
            if rounded_state_signal == settled_state_signal:
                num_converged_points += 1
            else:
                num_converged_points = 1
                settled_state_signal = rounded_state_signal
        # if state_signal is not near an integer, start over
        else:
            num_converged_points = 0
            settled_state_signal = None
        i += 1
        if num_converged_points >= const.WD_STATE_CONVERGING_COUNT:
            break
            # return the integer state_signal is near and the index after the last one
            # looked at in this function
    return settled_state_signal, i


def get_wd_state_time_intervals(time_domain, state_signal_data, end_time):
    first_state_signal, i = _get_next_state_signal(state_signal_data, 0)
    wd_state_time_intervals = [StateInterval(first_state_signal, time_domain[0], None)]
    while i < len(state_signal_data):
        state_signal = state_signal_data[i]

        # if state_signal changes significantly from the last recorded watchdog state
        # assume this is a transition and record it
        if abs(state_signal - wd_state_time_intervals[-1].state) > const.WD_STATE_CONVERGING_EPSILON:
            transition_time = time_domain[i]
            next_state_signal, i = _get_next_state_signal(state_signal_data, i)  # effectively increments i
            wd_state_time_intervals[-1].end_time = transition_time
            wd_state_time_intervals.append(StateInterval(next_state_signal, transition_time, None))
            continue
        i += 1
    wd_state_time_intervals[-1].end_time = end_time
    return wd_state_time_intervals




# (USE matplotlib.cbook.flatten)
###
##
## @param l      multi-dimensional list
##
##Returns generator representing a flattened version of <em>l</em>. 
##
##@code
##    >>> import util
##    >>> l=[[[[1,2],[3]],[4,5,6]],[7,8,9]]
##    >>> [x for x in util.flatten(l)]
##    [1, 2, 3, 4, 5, 6, 7, 8, 9]
##@endcode
#def flatten(l):
#    for el in l:
#        if not isinstance(el,list):
#            yield el
#        else:
#            for i in flatten(el):
#                yield i

def binary_search(l, item):
    from bisect import bisect_left
    idx = bisect_left(l, item)
    if idx == len(l) or l[idx] != item:
        return None
    return idx

# vim: set ts=4 sw=4 expandtab smarttab:
